float4x4 ViewProjection;
float3 CameraPosition;

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(
	float4 vertexPosition : POSITION0,
	float4 voxelPositionSize : POSITION1,
	float4 voxelColor : COLOR0 )
{
	VertexShaderOutput output;

	float4 position = vertexPosition;

	float3 distance = voxelPositionSize.xyz - CameraPosition;
	position.xyz *= sign(distance);

	position.xyz *= voxelPositionSize.w;
	position.xyz -= voxelPositionSize.xyz;

	output.Position = mul(position, ViewProjection);
	output.Color = voxelColor;

	return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	//return 1;
	return input.Color;
}

technique Technique1
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();

		CullMode = None;
		ZEnable = True;
		ZWriteEnable = True;
		ZFunc = LessEqual;
	}
}
