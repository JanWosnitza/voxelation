float4x4 ViewProjection;

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(
	float4 position : POSITION0,
	float4 color : COLOR0 )
{
	VertexShaderOutput output;

	output.Position = mul(position, ViewProjection);
	output.Color = color;

	return output;
}

float4 PixelShaderFunction( float4 color : COLOR0 ) : COLOR0
{
	return color;
}

technique Technique1
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexShaderFunction();
		PixelShader = compile ps_3_0 PixelShaderFunction();

		CullMode = None;
		ZEnable = True;
		ZWriteEnable = True;
		ZFunc = LessEqual;
	}
}
