﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation
{
    public class Camera
    {
        private Vector3 _position = Vector3.Zero;

        public Vector3 Position
        {
            get { return this._position; }
            set
            {
                if ( this._position != value )
                {
                    this._position = value;
                    this._updateViewProjection = true;
                }
            }
        }

        private Quaternion _rotation = Quaternion.Identity;

        public Quaternion Rotation
        {
            get { return this._rotation; }
            set
            {
                if ( this._rotation != value )
                {
                    this._rotation = value;
                    this._updateViewProjection = true;
                }
            }
        }

        private float _fov = MathHelper.ToRadians( 60f );

        public float Fov
        {
            get { return _fov; }
            set
            {
                if ( this._fov != value )
                {
                    this._fov = value;
                    this._updateViewProjection = true;
                }
            }
        }

        private float _aspectRatio = 1f;

        public float AspectRatio
        {
            get { return _aspectRatio; }
            set
            {
                if ( this._aspectRatio != value )
                {
                    this._aspectRatio = value;
                    this._updateViewProjection = true;
                }
            }
        }


        private float _nearPlane = 0.1f;

        public float NearPlane
        {
            get { return _nearPlane; }
            set
            {
                if ( this._nearPlane != value )
                {
                    _nearPlane = value;
                    this._updateViewProjection = true;
                }
            }
        }

        private float _farPlane = 100f;

        public float FarPlane
        {
            get { return _farPlane; }
            set
            {
                if ( this._farPlane != value )
                {
                    _farPlane = value;
                    this._updateViewProjection = true;
                }
            }
        }

        public Vector3 Forward
        {
            get { return Vector3.Transform( Vector3.Forward, this.Rotation ); }
        }

        private bool _updateViewProjection = true;
        private Matrix _viewProjection = Matrix.Identity;

        public Matrix GetViewProjection()
        {
            if ( !this._updateViewProjection )
                return this._viewProjection;

            this._updateViewProjection = false;

            Matrix viewRotation, viewTranslation, view;
            Matrix.CreateFromQuaternion( ref this._rotation, out viewRotation );
            Matrix.CreateTranslation( ref this._position, out viewTranslation );
            Matrix.Multiply( ref viewTranslation, ref viewRotation, out view );

            Matrix projection;
            Matrix.CreatePerspectiveFieldOfView(
                this._fov,
                this._aspectRatio,
                this._nearPlane, this._farPlane,
                out projection);

            Matrix viewProjection;
            Matrix.Multiply( ref view, ref projection, out viewProjection );

            this._viewProjection = viewProjection;
            return viewProjection;
        }
    }
}
