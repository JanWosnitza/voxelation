﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using voxelation.Voxel.Graphics;
using voxelation.Voxel;
using LibNoise.Primitive;
using voxelation.Voxel.VolumeGenerators;
using voxelation.Voxel.ColorGenerator;

namespace voxelation
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private GraphicsDevice _device;
        private SpriteBatch _spriteBatch;

        private SpriteFont _font;

        public Game1()
        {
            this._graphics = new GraphicsDeviceManager( this );
            this.Content.RootDirectory = "Content";

            this.Window.AllowUserResizing = true;
            this.IsMouseVisible = true;

            this.InactiveSleepTime = TimeSpan.FromSeconds( 1.0 / 15.0 );
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this._device = _graphics.GraphicsDevice;

            var bounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            this._graphics.PreferredBackBufferWidth = bounds.Width;
            this._graphics.PreferredBackBufferWidth = bounds.Height;

            this._camera = new Camera
            {
                Fov = MathHelper.ToRadians( 60f ),
                NearPlane = 0.01f,
                FarPlane = 5f,
                Position = new Vector3( 0f, 0f, -1f ),
            };

            // Create a new SpriteBatch, which can be used to draw textures.
            this._spriteBatch = new SpriteBatch( GraphicsDevice );

            this._font = this.Content.Load<SpriteFont>( "Font" );

            this._defaultEffect = this.Content.Load<Effect>( "default" );

            this._graphics.PreferMultiSampling = true;

            this._voxelManager = new VoxelManager()
            {
                Volume = new Torus() { Radius = 0.3f, Thickness = 0.1f, },
                //Volume = new SimplexPerlinLandscape() { Frequenzy = Vector2.One * 3, Height = 0.25f, Error = 0.75f },
                //Volume = new Sphere() { Radius = 0.3f },
                //Volume = new RomanSurface(),
                //Volume = new TestVolume(),
            };

            this._renderer = new VoxelRenderer(
                this._device,
                this.Content.Load<Effect>( "voxel" ),
                new DynamicInstanceArray<Voxel.Graphics.VertexVoxelInstanceData>( 1000000 ),
                this._voxelManager )
            {
                Color = new SimplexPerlin3d() { Frequenzy = Vector3.One * 1.5f, },
            };
        }

        private VoxelManager _voxelManager;

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        private Vector2 DeadZone( Vector2 v, double pow = 1.0 )
        {
            const float min = 0.25f;

            v.X = (float) Math.Pow( Math.Max( 0f, Math.Abs( v.X ) - min ) / ( 1f - min ), pow ) * Math.Sign( v.X );
            v.Y = (float) Math.Pow( Math.Max( 0f, Math.Abs( v.Y ) - min ) / ( 1f - min ), pow ) * Math.Sign( v.Y );

            return v;
        }

        private float time = 0f;

        private int _preferedDepth = 7;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update( GameTime gameTime )
        {
            var key = Keyboard.GetState();

            if ( ( key.IsKeyDown( Keys.LeftAlt ) || key.IsKeyDown( Keys.RightAlt ) ) &&
                 key.IsKeyDown( Keys.Enter ) )
            {
                if ( !ignoreFullScreenToggle )
                {
                    if ( this._graphics.IsFullScreen )
                    {
                        this._graphics.PreferredBackBufferWidth = 1024;
                        this._graphics.PreferredBackBufferHeight = 786;
                    }
                    else
                    {
                        var bounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                        this._graphics.PreferredBackBufferWidth = bounds.Width;
                        this._graphics.PreferredBackBufferHeight = bounds.Height;
                    }

                    this._graphics.ToggleFullScreen();
                    ignoreFullScreenToggle = true;
                }
            }
            else
                ignoreFullScreenToggle = false;

            var pad = GamePad.GetState( PlayerIndex.One, GamePadDeadZone.None );

            // Allows the game to exit
            if ( pad.Buttons.Back == ButtonState.Pressed )
                this.Exit();

            var deltaTime = (float) gameTime.ElapsedGameTime.TotalSeconds;

            var left = DeadZone( pad.ThumbSticks.Left, 1.2 );
            var right = DeadZone( pad.ThumbSticks.Right );

            var move = new Vector3( -left.X, pad.Triggers.Left - pad.Triggers.Right, left.Y ) * deltaTime;
            var rotate = new Vector3( right.X, -right.Y, 0 ) * deltaTime * 2f;

            if ( pad.Buttons.A == ButtonState.Pressed )
                move *= 2;

            //this._camera.Rotation.
            this._rotation += rotate;
            this._rotation.Y = MathHelper.Clamp( this._rotation.Y, -MathHelper.PiOver2, MathHelper.PiOver2 );

            this._camera.Rotation = Quaternion.CreateFromAxisAngle( Vector3.UnitX, this._rotation.Y ) * Quaternion.CreateFromAxisAngle( Vector3.Up, this._rotation.X ); //, this._rotation.Y, this._rotation.Z );
            this._camera.Position += Vector3.Transform( move, Quaternion.Inverse( this._camera.Rotation ) );

            var updateVoxels = false;

            var wasAnyPressed = this.anyPressed;
            this.anyPressed = false;
            if ( pad.Buttons.RightShoulder == ButtonState.Pressed )
            {
                this.anyPressed = true;
                if ( !wasAnyPressed )
                {
                    updateVoxels = true;
                    this._preferedDepth++;
                }
            }

            if ( pad.Buttons.LeftShoulder == ButtonState.Pressed )
            {
                this.anyPressed = true;
                if ( !wasAnyPressed )
                {
                    updateVoxels = true;
                    this._preferedDepth--;
                }
            }

            if ( this._voxelManager.Depth < 1 )
                this._preferedDepth = 1;
            else if ( this._voxelManager.Depth > 12 )
                this._preferedDepth = 12;

            if ( pad.Buttons.B == ButtonState.Pressed )
            {
                //if ( this._voxelManager.Depth > 7 )
                //    this._voxelManager.Depth = 7;

                if ( pad.Buttons.Y == ButtonState.Released )
                    time += (float) gameTime.ElapsedGameTime.TotalSeconds;
                else
                    time += (float) gameTime.ElapsedGameTime.TotalSeconds * 4f;

                var phase = time * 0.25f;

                var torus = this._voxelManager.Volume as Torus;
                if ( torus != null )
                    torus.Rotation = Quaternion.CreateFromYawPitchRoll( phase, phase * 0.7f, phase * 0.4f );

                var roman = this._voxelManager.Volume as RomanSurface;
                if ( roman != null )
                    roman.Rotation = Quaternion.CreateFromYawPitchRoll( phase, phase * 0.7f, phase * 0.4f );

                var sphere = this._voxelManager.Volume as Sphere;
                if ( sphere != null )
                {
                    sphere.Center.X = (float) ( Math.Cos( phase ) * 0.3 );
                    sphere.Center.Y = (float) ( Math.Sin( phase * 5 ) * 0.2 );
                    sphere.Center.Z = (float) ( Math.Sin( phase ) * 0.3 );
                }

                var landscape = this._voxelManager.Volume as SimplexPerlinLandscape;
                if ( landscape != null )
                    landscape.time = phase;

                var test = this._voxelManager.Volume as TestVolume;
                if ( test != null )
                    test.Time = time;

                if ( !gameTime.IsRunningSlowly )
                    updateVoxels = true;
            }
            else
            {
                if ( this._voxelManager.Depth != this._preferedDepth )
                {
                    updateVoxels = true;
                    this._voxelManager.Depth = this._preferedDepth;
                }

                /*if ( !gameTime.IsRunningSlowly )
                {
                    if ( this._voxelManager.Depth < this._preferedDepth )
                    {
                        updateVoxels = true;
                        this._voxelManager.Depth++;
                    }
                    else if ( this._voxelManager.Depth > this._preferedDepth )
                    {
                        updateVoxels = true;
                        this._voxelManager.Depth = this._preferedDepth;
                    }
                }*/
            }

            /*if ( ( this._ignoredFrames > 30 ) && gameTime.IsRunningSlowly )
            {
                this._ignoredFrames = 0;

                updateVoxels = true;
                this._voxelManager.Depth--;
            }*/

            //if ( updateVoxels )

            this._voxelManager.Update();

            this._ignoredFrames++;

            base.Update( gameTime );
        }

        private Vector3 _rotation = Vector3.Zero;

        private bool ignoreFullScreenToggle = false;

        private bool anyPressed = false;

        private Camera _camera;

        public VoxelRenderer _renderer;

        private DateTime lastFpsUpdate = DateTime.MinValue;
        private int frames = 0;
        private string strFps = "";

        private VertexPositionColor[] Cube = new VertexPositionColor[]
        {
            new VertexPositionColor( new Vector3( -0, -0, -0 ), Color.Green ),
            new VertexPositionColor( new Vector3( -1, -0, -0 ), Color.Green ),

            new VertexPositionColor( new Vector3( -0, -0, -0 ), Color.Red ),
            new VertexPositionColor( new Vector3( -0, -1, -0 ), Color.Red ),

            new VertexPositionColor( new Vector3( -0, -0, -0 ), Color.Blue ),
            new VertexPositionColor( new Vector3( -0, -0, -1 ), Color.Blue ),

            new VertexPositionColor( new Vector3( -1, -1, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -0, -1, -1 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -1, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -1, -0, -1 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -1, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -1, -1, -0 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -0, -1, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -0, -1, -0 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -0, -1, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -0, -0, -1 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -0, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -1, -0, -0 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -0, -1 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -0, -0, -1 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -1, -0 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -1, -0, -0 ), Color.Gray ),

            new VertexPositionColor( new Vector3( -1, -1, -0 ), Color.Gray ),
            new VertexPositionColor( new Vector3( -0, -1, -0 ), Color.Gray ),
        };
        private Effect _defaultEffect;

        private int _ignoredFrames = 0;

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw( GameTime gameTime )
        {
            this._ignoredFrames = 0;

            this._device.Clear( new Color( 0, 0, 0 ) );

            var bounds = Window.ClientBounds;
            this._camera.AspectRatio = (float) bounds.Width / (float) bounds.Height;

            this._defaultEffect.Parameters[ "ViewProjection" ].SetValue( this._camera.GetViewProjection() );
            foreach ( var pass in this._defaultEffect.CurrentTechnique.Passes )
            {
                pass.Apply();
                this._device.DrawUserPrimitives( PrimitiveType.LineList, Cube, 0, Cube.Length / 2 );
            }

            this._renderer.Draw( this._camera );

            this._spriteBatch.Begin();

            var now = DateTime.Now;

            frames++;
            var diff = ( now - lastFpsUpdate ).TotalSeconds;
            if ( diff > 0.25 )
            {
                this.strFps = ( frames / diff ).ToString( "f1" ) + " FPS";

                lastFpsUpdate = now;
                frames = 0;
            }

            var resolution = ( 1 << ( this._voxelManager.Depth - 1 ) );
            this._spriteBatch.DrawString( this._font, this.strFps, new Vector2( 10, 10 ), Color.White );
            this._spriteBatch.DrawString( this._font,
                "Voxels: 8^" + ( this._voxelManager.Depth - 1 ) + " / " + this._renderer.VoxelData.Length + " = " + ( (float) this._renderer.VoxelData.Length / (float) ( resolution * resolution * resolution ) ).ToString( "p1" ), new Vector2( 10, 30 ), Color.White );
            this._spriteBatch.DrawString( this._font, this._voxelManager.Volume.GetType().Name, new Vector2( 10, 50 ), Color.White );

            this._spriteBatch.End();

            base.Draw( gameTime );
        }
    }
}
