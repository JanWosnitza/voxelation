﻿using LibNoise;
using LibNoise.Primitive;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.ColorGenerator
{
    public class SimplexPerlin3d : IColorGenerator
    {
        public Vector3 Frequenzy = Vector3.One;

        private SimplexPerlin _red;
        private SimplexPerlin _green;
        private SimplexPerlin _blue;

        public SimplexPerlin3d( NoiseQuality quality = NoiseQuality.Standard, int seedRed = 0, int seedGreen = 1, int seedBlue = 2 )
        {
            this._red = new SimplexPerlin( seedRed, quality );
            this._green = new SimplexPerlin( seedGreen, quality );
            this._blue = new SimplexPerlin( seedBlue, quality );

            this.GetColor = implGetColor;
        }

        public GetColor GetColor { get; private set; }

        public void implGetColor( ref Vector3 position, float size, ref Color color )
        {
            Vector3 o;
            Vector3.Multiply( ref position, ref Frequenzy, out o );

            color.R = (byte) ( _red.GetValue( o.X, o.Y, o.Z ) * 128 + 128 );
            color.G = (byte) ( _green.GetValue( o.X, o.Y, o.Z ) * 128 + 128 );
            color.B = (byte) ( _blue.GetValue( o.X, o.Y, o.Z ) * 128 + 128 );
            color.A = 255;
        }
    }
}
