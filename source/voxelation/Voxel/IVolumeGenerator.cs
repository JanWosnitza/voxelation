﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel
{
    public delegate VoxelState GetVolume( ref Vector3 position, float size );

    public interface IVolumeGenerator
    {
        GetVolume GetVolume { get; }
    }
}
