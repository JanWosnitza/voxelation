﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel
{
    public static class VoxelHelpers
    {
        public static uint ReverseBits( uint b )
        {
            return (uint) ( ReverseBits( (ushort) b ) << 16 ) |
                   (uint) ReverseBits( (ushort) ( b >> 16 ) );
        }

        public static ushort ReverseBits( ushort v )
        {
            return (ushort) ( ( ReverseBits( (byte) v ) << 8 ) |
                              ReverseBits( (byte) ( v >> 8 ) ) );
        }

        public static byte ReverseBits( byte b )
        {
            // Source: http://stackoverflow.com/questions/3587826/is-there-a-built-in-function-to-reverse-bit-order
            // TODO: verify speed

            return (byte) ( ( ( ( ( b * 0x0802u ) & 0x22110u ) | ( ( b * 0x8020u ) & 0x88440u ) ) * 0x10101u ) >> 16 );
        }
    }
}
