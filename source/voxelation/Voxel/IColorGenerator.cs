﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel
{
    public delegate void GetColor( ref Vector3 position, float size, ref Color color );

    public interface IColorGenerator
    {
        GetColor GetColor { get; }
    }
}
