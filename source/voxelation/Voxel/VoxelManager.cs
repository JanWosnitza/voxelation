﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using voxelation.Voxel.Graphics;

namespace voxelation.Voxel
{
    public class VoxelManager
    {
        public VoxelManager()
        { }

        public VoxelNode Root
        { get { return _root; } }

        private VoxelNode _root = new VoxelNode();

        public IVolumeGenerator Volume { get; set; }

        public int Depth
        {
            get { return this._depth; }
            set
            {
                if ( this._depth != value )
                {
                    if ( value < 1 )
                        this._depth = 1;
                    else
                        this._depth = value;
                }
            }
        }

        private int _depth = 7;
        private int _version = 0;

        public int Version
        { get { return this._version; } }

        private VoxelNode _getVoxelsHelper = new VoxelNode();

        public void Update()
        {
            VoxelNode.UpdateChildrenInfo info;

            info.Position = new VoxelPosition { Scale = 1, };
            info.GetVolume = this.Volume.GetVolume;
            info.Depth = this._depth;
            info.Version = ++this._version;
            info.Manager = this;
            info.AnyChanges = false;

            this._root.UpdateState( ref info );
            this._root.UpdateChildren( ref info );

            this._getVoxelsHelper.State = VoxelState.IsFilled;
            this._getVoxelsHelper.Occlusion = VoxelOcclusion.None;

            this.Root.SetNotOccluded(
                _getVoxelsHelper, _getVoxelsHelper,
                _getVoxelsHelper, _getVoxelsHelper,
                _getVoxelsHelper, _getVoxelsHelper,
                this.Version );


            //if ( !info.AnyChanges )
            //    this._version--;
        }

        public void RemoveVoxel( ref InstanceId id )
        {
            _renderer.RemoveVoxel( ref id );
        }

        private VoxelRenderer _renderer;

        public void AddView( VoxelRenderer renderer )
        {
            this._renderer = renderer;
        }
    }
}
