﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel
{
    public struct VoxelPosition : IEquatable<VoxelPosition>
    {
        public ushort X;
        public ushort Y;
        public ushort Z;
        public ushort Scale;

        public void StepIn()
        {
            this.X <<= 1;
            this.Y <<= 1;
            this.Z <<= 1;
            this.Scale <<= 1;
        }

        public void StepOut()
        {
            this.X >>= 1;
            this.Y >>= 1;
            this.Z >>= 1;
            this.Scale >>= 1;
        }

        public void NextDepth( out VoxelPosition ret, int x, int y, int z )
        {
            ret.X = (ushort) ( ( X << 1 ) + x );
            ret.Y = (ushort) ( ( Y << 1 ) + y );
            ret.Z = (ushort) ( ( Z << 1 ) + z );
            ret.Scale = (ushort) ( this.Scale << 1 );
        }

        public bool GetIndexAndUpdateMask( ref int mask, out int index )
        {
            if ( mask > 1 )
            {
                mask >>= 1;
                index = ( ( this.X & mask ) | ( ( this.Y & mask ) << 1 ) | ( this.Z & mask ) << 2 ) / mask;

                return true;
            }

            index = -1;
            return false;
        }

        public float ToPositionSize( out Vector3 position )
        {
            var size = 1f / this.Scale;
            var offset = size * 0.5f - 0.5f;

            position.X = offset + this.X * size;
            position.Y = offset + this.Y * size;
            position.Z = offset + this.Z * size;

            return size;
        }

        public void ToPositionSize( out Vector4 positionSize )
        {
            var size = 1f / this.Scale;
            var offset = size * 0.5f;

            positionSize.X = offset + this.X * size;
            positionSize.Y = offset + this.Y * size;
            positionSize.Z = offset + this.Z * size;
            positionSize.W = size;
        }

        public bool Equals( VoxelPosition other )
        {
            return ( this.Scale == other.Scale ) &&
                   ( this.X == other.X ) &&
                   ( this.Y == other.Y ) &&
                   ( this.Z == other.Z );
        }
    }
}
