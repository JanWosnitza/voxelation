﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class TestVolume : IVolumeGenerator
    {
        public TestVolume()
        {
            GetVolume = this.implGetVolume;
        }

        public GetVolume GetVolume { get; private set; }

        public float Time;

        private VoxelState implGetVolume( ref Microsoft.Xna.Framework.Vector3 position, float size )
        {
            var a = Time % 1.5;
            if ( a < 0.25 )
                return VoxelState.None;

            if ( size > 0.4 )
                return VoxelState.IsFilledWithChildren;

            var p = ( position.X + Time ) % 0.5;
            if ( p < 0.25 )
                return VoxelState.IsFilled;

            return VoxelState.None;
        }
    }
}
