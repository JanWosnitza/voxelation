﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class Sphere : IVolumeGenerator
    {
        public Vector3 Center;
        public float Radius;

        public Sphere()
        {
            this.GetVolume = implGetVolume;
        }

        public GetVolume GetVolume { get; private set; }

        public VoxelState implGetVolume( ref Vector3 position, float size )
        {
            float distance;
            Vector3.Distance( ref this.Center, ref position, out distance );

            return VolumeHelper.GetVolumeFromDistanceToSurface( distance - this.Radius, size );
        }
    }
}
