﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class Cube : IVolumeGenerator
    {
        public Cube()
        {
            this.GetVolume = implGetVolume;
        }

        public GetVolume GetVolume { get; private set; }

        public Vector3 Center = new Vector3( 0.25f, 0.25f, 0.25f );
        public float Size = 0.5f;

        public Quaternion Rotation = Quaternion.Identity;

        private Random random = new Random();

        private VoxelState implGetVolume( ref Vector3 position, float size )
        {
            Vector3 localTemp;
            Vector3.Subtract( ref position, ref Center, out localTemp );
            
            Vector3 local;
            Vector3.Transform( ref localTemp, ref Rotation, out local );

            var x = Math.Abs( local.X );
            var y = Math.Abs( local.Y );
            var z = Math.Abs( local.Z );
            var distance = Math.Max( x, Math.Max( y, z ) ) - Size * 0.5f;

            return VolumeHelper.GetVolumeFromDistanceToSurface( distance, size );
        }
    }
}
