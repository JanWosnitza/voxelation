﻿using LibNoise;
using LibNoise.Primitive;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class SimplexPerlinLandscape : IVolumeGenerator
    {
        public SimplexPerlinLandscape( int seed = 0, NoiseQuality quality = NoiseQuality.Best)
        {
            this.GetVolume = implGetVolume;

            _noise = new SimplexPerlin(seed, quality );
        }

        public Vector2 Frequenzy = Vector2.One;
        public float Height = 1f;
        public float Error = 0.5f;

        public float time;

        public GetVolume GetVolume { get; private set; }

        private SimplexPerlin _noise;

        public VoxelState implGetVolume( ref Vector3 position, float size )
        {
            var height = _noise.GetValue( position.X * Frequenzy.X, position.Z * Frequenzy.Y, time );

            return VolumeHelper.GetVolumeFromDistanceToSurface( -position.Y + height * Height * 0.5f, size, Error );
        }
    }
}
