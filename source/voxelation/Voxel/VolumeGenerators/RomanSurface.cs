﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class RomanSurface : IVolumeGenerator
    {
        public RomanSurface()
        {
            this.GetVolume = implGetVolume;
        }

        public Vector3 Center;
        public Quaternion Rotation = Quaternion.Identity;

        public float Radius = 0.75f;

        public GetVolume GetVolume { get; private set; }

        private VoxelState implGetVolume( ref Vector3 position, float size )
        {
            const float epsilon = 0.00001f;
            Vector3 localTemp;
            Vector3.Subtract( ref position, ref Center, out localTemp );

            if ( ( localTemp.X * localTemp.X < epsilon * epsilon ) &&
                 ( localTemp.Y * localTemp.Y < epsilon * epsilon ) &&
                 ( localTemp.Z * localTemp.Z < epsilon * epsilon ) )
                return VoxelState.IsFilledWithChildren;

            Vector3 local;
            Vector3.Transform( ref localTemp, ref this.Rotation, out local );

            // http://en.wikipedia.org/wiki/Roman_surface

            var x2 = local.X * local.X;
            var y2 = local.Y * local.Y;
            var z2 = local.Z * local.Z;

            var distance = x2 * y2 + y2 * z2 + z2 * x2 - Radius * Radius * local.X * local.Y * local.Z;

            return VolumeHelper.GetVolumeFromDistanceToSurface( (float) distance, size );
        }
    }
}
