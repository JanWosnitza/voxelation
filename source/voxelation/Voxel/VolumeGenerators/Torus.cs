﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public class Torus : IVolumeGenerator
    {
        public Vector3 Center = Vector3.Zero;
        public Quaternion Rotation = Quaternion.Identity;

        public float Radius = 0.3f;
        public float Thickness = 0.2f;

        public Torus()
        {
            this.GetVolume = implGetVolume;
        }

        public GetVolume GetVolume { get; private set; }

        public VoxelState implGetVolume( ref Vector3 position, float size )
        {
            const float epsilon = 0.00001f;
            Vector3 localTemp;
            Vector3.Subtract( ref position, ref Center, out localTemp );

            if ( ( localTemp.X * localTemp.X < epsilon * epsilon ) &&
                 ( localTemp.Y * localTemp.Y < epsilon * epsilon ) &&
                 ( localTemp.Z * localTemp.Z < epsilon * epsilon ) )
                return VoxelState.IsFilledWithChildren;

            Vector3 local;
            Vector3.Transform( ref localTemp, ref this.Rotation, out local );

            // http://en.wikipedia.org/wiki/Torus
            //(R-sqrt(x^2+y^2))^2+z^2 = r^2

            var a = Radius - Math.Sqrt( local.X * local.X + local.Z * local.Z );
            var distance = Math.Sqrt( a * a + local.Y * local.Y ) - Thickness;

            return VolumeHelper.GetVolumeFromDistanceToSurface( (float) distance, size );
        }
    }
}
