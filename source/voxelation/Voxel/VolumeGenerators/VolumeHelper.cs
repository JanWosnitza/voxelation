﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.VolumeGenerators
{
    public static class VolumeHelper
    {
        private const float sqrt3half = 0.86602540378443864676372317075294f;

        public static VoxelState GetVolumeFromDistanceToSurface( float distance, float voxelSize, float errorFactor )
        {
            return GetVolumeFromDistanceToSurface( distance, voxelSize * ( 1 + errorFactor ) );
        }

        public static VoxelState GetVolumeFromDistanceToSurface( float distance, float voxelSize )
        {
            var radius = voxelSize * sqrt3half;

            if ( distance <= 0f )
            {
                if ( -distance >= radius )
                    return VoxelState.IsFilled;

                return VoxelState.IsFilledWithChildren;
            }
            else
            {
                if ( distance <= radius )
                    return VoxelState.HasChildren;

                return VoxelState.None;
            }
        }
    }
}
