﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation
{
    public class CubeGeometry
    {
        public GraphicsDevice Device { get; private set; }
        public VertexBuffer VertexBuffer { get; private set; }
        public IndexBuffer IndexBuffer { get; private set; }

        private int _lockCount;

        private static Dictionary<GraphicsDevice, CubeGeometry> _geometry = new Dictionary<GraphicsDevice, CubeGeometry>();

        public static CubeGeometry CreateOrGet( GraphicsDevice device )
        {
            lock ( _geometry )
            {
                CubeGeometry ret;

                _geometry.TryGetValue( device, out ret );

                if ( ret != null )
                {
                    ret._lockCount++;
                    return ret;
                }

                ret = new CubeGeometry( device );

                ret.Device = device;


                ret._lockCount = 1;
                _geometry[ device ] = ret;

                return ret;
            }
        }

        //      4--------5
        //     /        /|
        //    2--------3 |
        //    |        | |
        //    |        | 6
        //    |        |/
        //    0--------1

        private CubeGeometry( GraphicsDevice device )
        {
            this.Device = device;

            this.VertexBuffer = new VertexBuffer( device, VertexVoxel.VertexDeclaration, 7, BufferUsage.None );
            this.VertexBuffer.SetData( new VertexVoxel[]
            {
                new VertexVoxel( new Vector3( -0.5f, -0.5f, +0.5f ) ), // 0
                new VertexVoxel( new Vector3( +0.5f, -0.5f, +0.5f ) ), // 1
                new VertexVoxel( new Vector3( -0.5f, +0.5f, +0.5f ) ), // 2
                new VertexVoxel( new Vector3( +0.5f, +0.5f, +0.5f ) ), // 3
                new VertexVoxel( new Vector3( -0.5f, +0.5f, -0.5f ) ), // 4
                new VertexVoxel( new Vector3( +0.5f, +0.5f, -0.5f ) ), // 5
                new VertexVoxel( new Vector3( +0.5f, -0.5f, -0.5f ) ), // 6
            } );

            this.IndexBuffer = new IndexBuffer( device, IndexElementSize.SixteenBits, 18, BufferUsage.None );
            this.IndexBuffer.SetData( new short[]
            {
                0, 1, 2,
                1, 2, 3,

                2, 3, 4,
                3, 4, 5,

                5, 6, 3,
                6, 3, 1,
            } );
        }

        public void Draw( int instanceCount )
        {
            this.Device.DrawInstancedPrimitives( PrimitiveType.TriangleList, 0, 0, 7, 0, 6, instanceCount );
        }

        public void Release()
        {
            lock ( _geometry )
            {
                this._lockCount--;

                if ( this._lockCount != 0 )
                    return;

                _geometry.Remove( this.Device );

                this.VertexBuffer.Dispose();
                this.IndexBuffer.Dispose();

                this.VertexBuffer = null;
                this.IndexBuffer = null;
            }
        }

        private struct VertexVoxel : IVertexType
        {
            public VertexVoxel( Vector3 position )
            {
                this.Position = position;
            }

            public Vector3 Position;

            public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
                new VertexElement( 0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0 )
            );

            VertexDeclaration IVertexType.VertexDeclaration
            {
                get { return VertexDeclaration; }
            }
        }
    }
}
