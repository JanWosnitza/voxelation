﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.Graphics
{
    public struct VertexVoxelInstanceData : IVertexType
    {
        public Vector4 PositionSize;
        public Vector4 Color;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement( sizeof( float ) * 0, VertexElementFormat.Vector4, VertexElementUsage.Position, 1 ),
            new VertexElement( sizeof( float ) * 4, VertexElementFormat.Vector4, VertexElementUsage.Color, 0 )
        );

        public static readonly int SizeOf = sizeof( float ) * 8;

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }
    }
}
