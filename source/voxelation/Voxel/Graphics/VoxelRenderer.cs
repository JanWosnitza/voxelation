﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using voxelation.Voxel.Graphics;

namespace voxelation.Voxel.Graphics
{
    public class VoxelRenderer : IDisposable
    {
        private const int MaxVertices = ushort.MaxValue - 1;
        private const int PrimitivesPerCube = 6 + 4;

        public VoxelRenderer(
            GraphicsDevice device,
            Effect effect,
            DynamicInstanceArray<VertexVoxelInstanceData> voxelData,
            VoxelManager manager )
        {
            this._device = device;
            this._effect = effect;

            this._geometry = CubeGeometry.CreateOrGet( device );

            this._instanceVertexBuffer = new DynamicVertexBuffer( this._device, VertexVoxelInstanceData.VertexDeclaration, voxelData.Capacity, BufferUsage.None );
            this._instanceVertexBuffer.ContentLost += _instanceVertexBuffer_ContentLost;

            this.VoxelData = voxelData;
            this.Manager = manager;
            manager.AddView( this );

            this._vertexBuffers = new VertexBufferBinding[]
            {
                new VertexBufferBinding( this._geometry.VertexBuffer ),
                new VertexBufferBinding( this._instanceVertexBuffer, 0, 1 ),
            };
        }

        private void _instanceVertexBuffer_ContentLost( object sender, EventArgs e )
        {
            this._lastVoxelVersion = 0;
        }

        public DynamicInstanceArray<VertexVoxelInstanceData> VoxelData { get; private set; }
        public VoxelManager Manager { get; private set; }

        ~VoxelRenderer()
        {
            Dispose( false );
        }

        private int _lastVoxelVersion = 0;

        private void UpdateVoxels()
        {
            lock ( this._voxelId )
            {
                for ( int i = 0; i < this._removeIds.Count; i++ )
                    this.VoxelData.Remove( this._removeIds[ i ] );
                _removeIds.Clear();

                VoxelPosition position = default( VoxelPosition );
                position.Scale = 1;

                this.Manager.Root.RemoveVoxels( this );
                this.Manager.Root.GetVoxels( ref position, this );

                this._lastFrameVersion = this.Manager.Version;
            }
        }

        private void CommitChanges()
        {
            const int MaxEmptyToIgnore = 50;

            if ( ( this.VoxelData.Version != this._lastVoxelVersion ) &&
                 ( this.VoxelData.Length > 0 ) )
            {
                /*this._instanceVertexBuffer.SetData(
                    0,
                    this.VoxelData.Slots,
                    0,
                    this.VoxelData.Length,
                    VertexVoxelInstanceData.SizeOf );*/

                var lastVersion = this._lastVoxelVersion;
                this._lastVoxelVersion = this.VoxelData.Version;
                var slotVersions = this.VoxelData.SlotVersions;

                var startIndex = -1;
                var endIndex = -1;

                var emptyCounter = 0;
                for ( int i = 0; i < this.VoxelData.Length; i++ )
                {
                    if ( slotVersions[ i ] > lastVersion )
                    {
                        if ( startIndex == -1 )
                            startIndex = i;
                        else
                            endIndex = i;
                    }
                    else if ( startIndex != -1 )
                    {
                        if ( ++emptyCounter >= MaxEmptyToIgnore )
                        {
                            int count;

                            if ( endIndex == -1 )
                                count = 1;
                            else
                                count = endIndex - startIndex + 1;

                            //commitCount += count + SetDataWeight;
                            this._instanceVertexBuffer.SetData(
                                VertexVoxelInstanceData.SizeOf * startIndex,
                                this.VoxelData.Slots,
                                startIndex,
                                count,
                                VertexVoxelInstanceData.SizeOf );

                            startIndex = endIndex = -1;
                            emptyCounter = 0;
                        }
                    }
                }

                if ( startIndex != -1 )
                {
                    int count;

                    if ( endIndex == -1 )
                        count = 1;
                    else
                        count = endIndex - startIndex + 1;

                    //commitCount += count + SetDataWeight;
                    this._instanceVertexBuffer.SetData(
                        VertexVoxelInstanceData.SizeOf * startIndex,
                        this.VoxelData.Slots,
                        startIndex,
                        count,
                        VertexVoxelInstanceData.SizeOf );
                }
            }
        }

        private int _lastFrameVersion = 0;

        public void Draw( Camera camera )
        {
            //this.VoxelData.Clear();
            //this._lastFrameVersion = 0;

            UpdateVoxels();

            if ( this.VoxelData.Length == 0 )
                return;

            CommitChanges();

            this._device.SetVertexBuffers( this._vertexBuffers );
            this._device.Indices = this._geometry.IndexBuffer;

            this._effect.Parameters[ "ViewProjection" ].SetValue( camera.GetViewProjection() );
            this._effect.Parameters[ "CameraPosition" ].SetValue( camera.Position );

            foreach ( var pass in _effect.CurrentTechnique.Passes )
            {
                pass.Apply();

                this._geometry.Draw( this.VoxelData.Length );
            }
        }

        private GraphicsDevice _device;
        private Effect _effect;

        private CubeGeometry _geometry;
        private DynamicVertexBuffer _instanceVertexBuffer;

        private VertexBufferBinding[] _vertexBuffers;

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            Dispose( true );
        }

        protected virtual void Dispose( bool disposing )
        {
            if ( IsDisposed || !disposing )
                return;

            this._geometry.Release();
            this._instanceVertexBuffer.Dispose();

            this._geometry = null;
            this._instanceVertexBuffer = null;

            IsDisposed = true;
        }

        private Dictionary<VoxelPosition, InstanceId> _voxelId = new Dictionary<VoxelPosition, InstanceId>();
        private List<InstanceId> _removeIds = new List<InstanceId>();

        public void RemoveVoxel( ref InstanceId id )
        {
            if ( id.Id == -1 )
                return;

            lock ( this._removeIds )
            {
                this._removeIds.Add( id );
                id.Id = -1;
            }
        }

        public IColorGenerator Color;

        public void AddVoxel( ref VoxelPosition position, ref InstanceId id )
        {
            if ( id.Id != -1 )
                return;

            lock ( this._voxelId )
            {
                VertexVoxelInstanceData voxel;

                position.ToPositionSize( out voxel.PositionSize );
                //voxel.PositionSize.W *= 0.6f;

                Vector3 p;
                p.X = voxel.PositionSize.X;
                p.Y = voxel.PositionSize.Y;
                p.Z = voxel.PositionSize.Z;

                Color color = default( Color );
                Color.GetColor( ref p, voxel.PositionSize.W, ref color );
                voxel.Color.X = color.R / 255f;
                voxel.Color.Y = color.G / 255f;
                voxel.Color.Z = color.B / 255f;
                voxel.Color.W = color.A / 255f;

                id = this.VoxelData.Add( ref voxel );
            }
        }
    }
}
