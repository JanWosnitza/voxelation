﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace voxelation.Voxel.Graphics
{
    public struct InstanceId
    {
        public int Id;
    }

    public class DynamicInstanceArray<TData>
    {
        public int Capacity { get; private set; }

        public DynamicInstanceArray( int capacity )
        {
            this.Capacity = capacity;

            this._slots = new TData[ capacity ];
            this._slotIds = new int[ capacity ];
            this._idSlots = new int[ capacity ];
            this._slotVersions = new int[ capacity ];

            for ( var i = 0; i < this._idSlots.Length; i++ )
            {
                this._slotIds[ i ] = i;
                this._idSlots[ i ] = i;
            }

            this._length = 0;
        }

        public int Length
        { get { return this._length; } }

        public TData[] Slots
        { get { return this._slots; } }

        // TODO: improve change tracking
        public int Version
        { get { return this._version; } }

        public int[] SlotVersions
        { get { return this._slotVersions; } }

        public InstanceId Add( ref TData data )
        {
            if ( this._length >= _slots.Length )
                return new InstanceId { Id = -1 };

            var slot = this._length++;
            var slotId = this._slotIds[ slot ];

            this._slots[ slot ] = data;

            this._slotVersions[ slot ] = ++this._version;
            
            return new InstanceId
            {
                Id = slotId,
            };
        }

        public void Remove( InstanceId instanceId )
        {
            if ( instanceId.Id < 0 )
                return;

            if ( this._length == 0 )
                throw new InvalidOperationException( "Array is empty" );

            var removeId = instanceId.Id;
            var removeSlot = this._idSlots[ removeId ];

            if ( removeSlot >= this.Length )
                throw new InvalidOperationException();

            var lastSlot = --this._length;
            var lastId = this._slotIds[ lastSlot ];

            this._slotIds[ removeSlot ] = lastId;
            this._idSlots[ removeId ] = lastSlot;

            this._slotIds[ lastSlot ] = removeId;
            this._idSlots[ lastId ] = removeSlot;

            this._slots[ removeSlot ] = this._slots[ lastSlot ];
            this._slots[ lastSlot ] = default( TData );

            this._slotVersions[ removeSlot ] = ++this._version;
        }

        public void Clear()
        {
            this._length = 0;         
            Array.Clear( this._slots, 0, this._slots.Length );

            this._version++;
        }

        private TData[] _slots;
        private int[] _slotIds;
        private int[] _idSlots;
        private int _length;
        private int _version;
        private int[] _slotVersions;
    }
}
