﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using voxelation.Voxel.Graphics;
using voxelation.Voxel.VolumeGenerators;

namespace voxelation.Voxel
{
    // TODO: remove enum
    [Flags]
    public enum VoxelState : byte
    {
        None = 0x0,
        IsFilled = 0x1,
        HasChildren = 0x2,
        IsFilledWithChildren = IsFilled | HasChildren,
    }

    [Flags]
    public enum VoxelOcclusion : byte
    {
        None = 0x00,

        XNegative = 0x01,
        XPositive = 0x02,

        YNegative = 0x04,
        YPositive = 0x08,

        ZNegative = 0x10,
        ZPositive = 0x20,

        All = 0x3F,
    }

    public class VoxelNode
    {
        //public bool IsFilled;
        public VoxelState State;

        public VoxelOcclusion Occlusion = VoxelOcclusion.None;

        private VoxelNode[] _children = null;

        private InstanceId _instanceId = new InstanceId { Id = -1 };

        public bool IsOccluded = false;

        public bool AnyChanges = false;

        public struct UpdateChildrenInfo
        {
            public VoxelManager Manager;
            public int Depth;
            public VoxelPosition Position;
            public GetVolume GetVolume;
            public int Version;
            public bool AnyChanges;
        }

        public void UpdateState( ref UpdateChildrenInfo info )
        {
            Vector3 vPosition;
            var size = info.Position.ToPositionSize( out vPosition );

            var prevState = this.State;
            var newState = info.GetVolume( ref vPosition, size );

            //float distance;
            //Vector3.Distance( ref info.CameraPosition, ref vPosition, out distance );

            //var depth = info.Depth;

            //if ( size / distance < 1 / 100f )
            //    depth = 1;

            if ( info.Depth == 1 )
                newState &= ~VoxelState.HasChildren;

            if ( prevState == newState )
                return;

            this.AnyChanges = true;
            this.State = newState;

            //*
            if ( ( ( prevState & VoxelState.IsFilled ) != 0 ) &&
                 ( ( ( newState & VoxelState.IsFilled ) == 0 ) || ( ( newState & VoxelState.HasChildren ) != 0 ) ) )
            {
                if ( this._instanceId.Id != -1 )
                    info.Manager.RemoveVoxel( ref this._instanceId );
            }
            //*/

            if ( ( newState & VoxelState.HasChildren ) != 0 )
            {
                if ( ( prevState & VoxelState.HasChildren ) == 0 )
                {
                    this.SetChildren();
                }
            }
            else
            {
                if ( ( newState & VoxelState.IsFilled ) != 0 )
                {
                    this.Occlusion = VoxelOcclusion.All;
                }
                else
                {
                    this.Occlusion = VoxelOcclusion.None;
                }

                if ( ( prevState & VoxelState.HasChildren ) != 0 )
                {
                    this.ReleaseChildren( ref info );
                }
            }
        }

        public void UpdateChildren( ref UpdateChildrenInfo info )
        {
            if ( ( this.State & VoxelState.HasChildren ) == 0 )
                return;

            var children = this._children;
            var n0 = children[ 0 ];
            var n1 = children[ 1 ];
            var n2 = children[ 2 ];
            var n3 = children[ 3 ];
            var n4 = children[ 4 ];
            var n5 = children[ 5 ];
            var n6 = children[ 6 ];
            var n7 = children[ 7 ];

            info.Depth--;
            info.Position.StepIn();

            // Update children states
            n0.UpdateState( ref info );

            info.Position.X++;
            n1.UpdateState( ref info );

            info.Position.Y++;
            n3.UpdateState( ref info );

            info.Position.X--;
            n2.UpdateState( ref info );

            info.Position.Z++;
            n6.UpdateState( ref info );

            info.Position.X++;
            n7.UpdateState( ref info );

            info.Position.Y--;
            n5.UpdateState( ref info );

            info.Position.X--;
            n4.UpdateState( ref info );

            // UpdateChildren

            n4.UpdateChildren( ref info );
            info.Position.X++;

            n5.UpdateChildren( ref info );
            info.Position.Y++;

            n7.UpdateChildren( ref info );
            info.Position.X--;

            n6.UpdateChildren( ref info );
            info.Position.Z--;

            n2.UpdateChildren( ref info );
            info.Position.X++;

            n3.UpdateChildren( ref info );
            info.Position.Y--;

            n1.UpdateChildren( ref info );
            info.Position.X--;

            n0.UpdateChildren( ref info );

            info.Position.StepOut();
            info.Depth++;

            this.AnyChanges |=
                n0.AnyChanges |
                n1.AnyChanges |
                n2.AnyChanges |
                n3.AnyChanges |
                n4.AnyChanges |
                n5.AnyChanges |
                n6.AnyChanges |
                n7.AnyChanges;

            if ( this.AnyChanges )
            {
                var o0 = n0.Occlusion;
                var o1 = n1.Occlusion;
                var o2 = n2.Occlusion;
                var o3 = n3.Occlusion;
                var o4 = n4.Occlusion;
                var o5 = n5.Occlusion;
                var o6 = n6.Occlusion;
                var o7 = n7.Occlusion;

                this.Occlusion =
                    ( VoxelOcclusion.XNegative & o0 & o2 & o4 & o6 ) |
                    ( VoxelOcclusion.XPositive & o1 & o3 & o5 & o7 ) |

                    ( VoxelOcclusion.YNegative & o0 & o1 & o4 & o5 ) |
                    ( VoxelOcclusion.YPositive & o2 & o3 & o6 & o7 ) |

                    ( VoxelOcclusion.ZNegative & o0 & o1 & o2 & o3 ) |
                    ( VoxelOcclusion.ZPositive & o4 & o5 & o6 & o7 );
            }
        }

        public bool UpdateOcclusion(
            VoxelNode xNegative, VoxelNode xPositive,
            VoxelNode yNegative, VoxelNode yPositive,
            VoxelNode zNegative, VoxelNode zPositive,
            int version )
        {
            var anyChanges =
                xNegative.AnyChanges |
                xPositive.AnyChanges |

                yNegative.AnyChanges |
                yPositive.AnyChanges |

                zNegative.AnyChanges |
                zPositive.AnyChanges;

            if ( !anyChanges & !this.AnyChanges )
                return false;

            var occlusion =
                ( xNegative.Occlusion & VoxelOcclusion.XPositive ) |
                ( xPositive.Occlusion & VoxelOcclusion.XNegative ) |
                ( yNegative.Occlusion & VoxelOcclusion.YPositive ) |
                ( yPositive.Occlusion & VoxelOcclusion.YNegative ) |
                ( zNegative.Occlusion & VoxelOcclusion.ZPositive ) |
                ( zPositive.Occlusion & VoxelOcclusion.ZNegative );

            if ( ( occlusion & VoxelOcclusion.All ) == VoxelOcclusion.All )
            {
                SetOccluded( version );
            }
            else
            {
                SetNotOccluded(
                    xNegative, xPositive,
                    yNegative, yPositive,
                    zNegative, zPositive,
                    version );
            }

            return this.AnyChanges;
        }

        private void SetOccluded( int version )
        {
            if ( !this.IsOccluded )
            {
                this.IsOccluded = true;
                this.AnyChanges = true;
            }

            //*
            if ( ( this.State & VoxelState.HasChildren ) == 0 )
                return;

            this._children[ 0 ].SetOccluded( version );
            this._children[ 1 ].SetOccluded( version );
            this._children[ 2 ].SetOccluded( version );
            this._children[ 3 ].SetOccluded( version );
            this._children[ 4 ].SetOccluded( version );
            this._children[ 5 ].SetOccluded( version );
            this._children[ 6 ].SetOccluded( version );
            this._children[ 7 ].SetOccluded( version );
            //*/
        }

        public void SetNotOccluded(
            VoxelNode xNegative, VoxelNode xPositive,
            VoxelNode yNegative, VoxelNode yPositive,
            VoxelNode zNegative, VoxelNode zPositive,
            int version )
        {
            if ( this.IsOccluded )
            {
                this.IsOccluded = false;
                this.AnyChanges = true;
            }

            if ( ( this.State & VoxelState.HasChildren ) == 0 )
                return;

            var children = this._children;
            var n0 = children[ 0 ];
            var n1 = children[ 1 ];
            var n2 = children[ 2 ];
            var n3 = children[ 3 ];
            var n4 = children[ 4 ];
            var n5 = children[ 5 ];
            var n6 = children[ 6 ];
            var n7 = children[ 7 ];

            // 0, 0, 0
            this.AnyChanges |= n0.UpdateOcclusion(
                GetChild( xNegative, 1 ), n1,
                GetChild( yNegative, 2 ), n2,
                GetChild( zNegative, 4 ), n4,
                version );

            // 1, 0, 0
            this.AnyChanges |= n1.UpdateOcclusion(
                n0, GetChild( xPositive, 0 ),
                GetChild( yNegative, 3 ), n3,
                GetChild( zNegative, 5 ), n5,
                version );

            // 0, 1, 0
            this.AnyChanges |= n2.UpdateOcclusion(
                GetChild( xNegative, 3 ), n3,
                n0, GetChild( yPositive, 0 ),
                GetChild( zNegative, 6 ), n6,
                version );

            // 1, 1, 0
            this.AnyChanges |= n3.UpdateOcclusion(
                n2, GetChild( xPositive, 2 ),
                n1, GetChild( yPositive, 1 ),
                GetChild( zNegative, 7 ), n7,
                version );

            // 0, 0, 1
            this.AnyChanges |= n4.UpdateOcclusion(
                GetChild( xNegative, 5 ), n5,
                GetChild( yNegative, 6 ), n6,
                n0, GetChild( zPositive, 0 ),
                version );

            // 1, 0, 1
            this.AnyChanges |= n5.UpdateOcclusion(
                n4, GetChild( xPositive, 4 ),
                GetChild( yNegative, 7 ), n7,
                n1, GetChild( zPositive, 1 ),
                version );

            // 0, 1, 1
            this.AnyChanges |= n6.UpdateOcclusion(
                GetChild( xNegative, 7 ), n7,
                n4, GetChild( yPositive, 4 ),
                n2, GetChild( zPositive, 2 ),
                version );

            // 1, 1, 1
            this.AnyChanges |= n7.UpdateOcclusion(
                n6, GetChild( xPositive, 6 ),
                n5, GetChild( yPositive, 5 ),
                n3, GetChild( zPositive, 3 ),
                version );
        }

        private VoxelNode GetChild( VoxelNode node, int index )
        {
            if ( ( node.State & VoxelState.HasChildren ) == 0 )
                return node;

            return node._children[ index ];
        }

        public void RemoveVoxels( VoxelRenderer renderer )
        {
            if ( !this.AnyChanges )
                return;

            if ( !this.IsOccluded )
                return;

            if ( this._instanceId.Id != -1 )
                renderer.RemoveVoxel( ref this._instanceId );

            if ( ( this.State & VoxelState.HasChildren ) == 0 )
                return;

            this._children[ 0 ].RemoveVoxels( renderer );
            this._children[ 1 ].RemoveVoxels( renderer );
            this._children[ 2 ].RemoveVoxels( renderer );
            this._children[ 3 ].RemoveVoxels( renderer );
            this._children[ 4 ].RemoveVoxels( renderer );
            this._children[ 5 ].RemoveVoxels( renderer );
            this._children[ 6 ].RemoveVoxels( renderer );
            this._children[ 7 ].RemoveVoxels( renderer );
        }


        public void GetVoxels( ref VoxelPosition position, VoxelRenderer renderer )
        {
            if ( !this.AnyChanges )
                return;

            this.AnyChanges = false;

            if ( this.IsOccluded )
                return;

            if ( ( this.State & VoxelState.HasChildren ) != 0 )
            {
                position.StepIn();

                // 0, 0, 0
                this._children[ 0 ].GetVoxels( ref position, renderer );

                position.X++; // 1, 0, 0
                this._children[ 1 ].GetVoxels( ref position, renderer );

                position.Y++; // 1, 1, 0

                this._children[ 3 ].GetVoxels( ref position, renderer );

                position.X--; // 0, 1, 0
                this._children[ 2 ].GetVoxels( ref position, renderer );

                position.Z++; // 0, 1, 1
                this._children[ 6 ].GetVoxels( ref position, renderer );

                position.X++; // 1, 1, 1
                this._children[ 7 ].GetVoxels( ref position, renderer );

                position.Y--; // 1, 0, 1
                this._children[ 5 ].GetVoxels( ref position, renderer );

                position.X--; // 0, 0, 1
                this._children[ 4 ].GetVoxels( ref position, renderer );

                position.StepOut();
            }
            else
            {
                if ( ( this.State & VoxelState.IsFilled ) != 0 )
                {
                    if ( this._instanceId.Id == -1 )
                        renderer.AddVoxel( ref position, ref this._instanceId );
                }
            }
        }

        public override string ToString()
        {
            return this.State.ToString();
        }

        private static Stack<VoxelNode[]> _nodeCache = new Stack<VoxelNode[]>();

        private void SetChildren()
        {
            if ( _nodeCache.Count > 0 )
            {
                this._children = _nodeCache.Pop();
            }
            else
            {
                this._children = new VoxelNode[]
                {
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                    new VoxelNode(),
                };
            }
        }

        private void ReleaseChildrenRecursion( ref UpdateChildrenInfo info )
        {
            this.ReleaseChildren( ref info );

            if ( ( ( this.State & VoxelState.IsFilled ) != 0 ) &&
                 ( ( this.State & VoxelState.HasChildren ) == 0 ) )
            {
                if ( this._instanceId.Id != -1 )
                    info.Manager.RemoveVoxel( ref _instanceId );
            }

            this.State = VoxelState.None;
            this.Occlusion = VoxelOcclusion.None;
            this.AnyChanges = false;
            this.IsOccluded = false;
        }

        private void ReleaseChildren( ref UpdateChildrenInfo info )
        {
            if ( this._children == null )
                return;

            this._children[ 0 ].ReleaseChildrenRecursion( ref info );
            this._children[ 1 ].ReleaseChildrenRecursion( ref info );
            this._children[ 2 ].ReleaseChildrenRecursion( ref info );
            this._children[ 3 ].ReleaseChildrenRecursion( ref info );
            this._children[ 4 ].ReleaseChildrenRecursion( ref info );
            this._children[ 5 ].ReleaseChildrenRecursion( ref info );
            this._children[ 6 ].ReleaseChildrenRecursion( ref info );
            this._children[ 7 ].ReleaseChildrenRecursion( ref info );

            _nodeCache.Push( this._children );
            this._children = null;
        }
    }
}
